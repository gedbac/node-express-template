'use strict';

function Post (post) {
	this.id = parseInt(post.id || 0, 10);
	this.title = post.title;
	this.body = post.body;
	this.created_at = post.created_at || new Date();
}

exports.Post = Post;