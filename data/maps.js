"use strict";

var sql = require('sql');

exports.post = sql.define({
	name: 'posts',
	columns: [
		'id',
		'title',
		'body',
		'created_at'
	]
});