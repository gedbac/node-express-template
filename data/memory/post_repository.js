"use strict";

var postCount = 1;

function PostRepository (dbUrl) {
    this.dbUrl = dbUrl;
}

PostRepository.prototype.dummyData = [];

PostRepository.prototype.findAll = function (callback) {
    callback(null, this.dummyData);
};

PostRepository.prototype.findById = function (id, callback) {
    var result = null,
        i;
    
    for (i = 0; i < this.dummyData.length; i += 1) {
        if (this.dummyData[i].id === id) {
            result = this.dummyData[i];
            break;
        }
    }
    
    callback(null, result);
};

PostRepository.prototype.save = function (posts, callback) {
    var post = null,
        i,
        j;
    
    if (typeof (posts.length) === 'undefined') {
        posts = [posts];
    }
    
    for (i = 0; i < posts.length; i += 1) {
        post = posts[i];
        post.id = postCount += 1;
        post.created_at = new Date();
        
        if (post.comments === undefined) {
            post.comments = [];
        }
        
        for (j = 0; j < post.comments.length; j += 1) {
            post.comments[j].created_at = new Date();
        }
        
        this.dummyData[this.dummyData.length] = post;
    }
    
    callback(null, posts);
};

PostRepository.prototype.remove = function (id, callback) {
    var result = null,
        i;
    
    for (i = 0; i < this.dummyData.length; i += 1) {
        if (this.dummyData[i].id === id) {
            this.dummyData.splice(i, 1);
            break;
        }
    }
    
    callback(null, true);
};

PostRepository.prototype.update = function (post, callback) {
    var i;
    
    for (i = 0; i < this.dummyData.length; i += 1) {
		
        if (this.dummyData[i].id === post.id) {
			console.log('post found!');
			
			this.dummyData[i].title = post.title;
			this.dummyData[i].body = post.body;
			
            break;
		}
	}
	
	callback(null, true);
};

/* Lets bootstrap with dummy data */
new PostRepository().save([
    {title: 'Post one', body: 'Body one', comments: [{
        author: 'Bob',
        comment: 'I love it'
    }, {
        author: 'Dave',
        comment: 'This is rubbish!'
    }]},
    {title: 'Post two', body: 'Body two'},
    {title: 'Post three', body: 'Body three'}
], function (error, posts) {});

exports.PostRepository = PostRepository;