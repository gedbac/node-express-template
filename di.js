'use strict';

module.exports = function (app) {

    var di = require('di');
    
    var PostRepository = require('./data/memory/post_repository').PostRepository;
    //var PostRepository = require('./data/postgresql/post_repository').PostRepository;
	
    var config = {
        'postProvider': ['type', PostRepository],
        'dbUrl': ['value', 'tcp://darius:123456@localhost:5432/node-express-template']
    };
    app.set('di', new di.Injector([config]));

};