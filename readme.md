# Standart Web Project Template

This standart web project template is based on **Node.js**. The main purpose of this project is to demonstrate how web application can be created using **Node.js**.

## Getting started

To run standart web project template on windows, prerequisites have to be installed:

* Node.js 0.10.* or newer
* Python 2.7.*

**Node.js** native addon build tool *'node-gyp'* has to be installded globally:

    npm install -g node-gyp

In order to build project, grunt's command line interface has to be installed globally.

    npm install -g grunt-cli

To install application dependencies, command *'npm install'* has to been executed in working directory:

    npm install

To start web server, such command has to be typed:

    grunt start

## FAQ

### Q: I cannot install any of the modules on windows that require compilation?

Visual C++ Redistributable for Visual Studio 2010 or newer has to be installed. Command *'npm install'* with aditional parameters can be executed:

    npm install --msvs_version=2012